##Install php and mysql extension
#FROM php:7.3-apache
#RUN apt-get update \
#    && apt-get install -y git
#
#RUN docker-php-ext-install mysqli
#RUN a2enmod rewrite
#
##Install Composer
#RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#RUN php composer-setup.php --install-dir=. --filename=composer
#RUN mv composer /usr/local/bin/
#
#COPY ./src/ /var/www/html
#RUN composer install
#EXPOSE 80

FROM composer:1.6.5 as build
WORKDIR /app
COPY ./src /app
RUN composer install

FROM php:7.3-apache
EXPOSE 80
COPY --from=build /app /app
COPY ./src/vhost.conf /etc/apache2/sites-available/000-default.conf
#RUN chown -R www-data:www-data /app \
RUN    a2enmod rewrite
