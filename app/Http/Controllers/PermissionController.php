<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\Permission as PermissionResource;

use Spatie\Permission\Models\Permission;
use DB;

class PermissionController extends Controller
{
    //
    public function index(Request $request) {
        $permisions = Permission::orderBy('id','DESC')->get();
        return PermissionResource::collection($permisions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
        ]);

        $permission = Permission::create(['name' => $request->input('name')]);

        return response()->json([
            'message' => 'Permission created successfully'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id) {

        $role = Permission::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")

            ->where("role_has_permissions.role_id",$id)

            ->get();


        return view('roles.show',compact('role','rolePermissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request) {

        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
        ]);


        $permission = Permission::find($request->input('id'));
        $permission->name = $request->input('name');
        $permission->save();


        return response()->json([
            'message' => 'Permission updated successfully'
        ]);

    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Request $request)

    {

        DB::table("permissions")->where('id',$request->input('id'))->delete();

        return response()->json([
            'message' => 'Permission deleted successfully'
        ]);

    }
}
