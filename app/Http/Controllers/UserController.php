<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Resources\Users as UserResource;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use DB;
use Hash;

class UserController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = User::orderBy('id','DESC')->get();
        return UserResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'roles' => 'required',
        ]);
        $input = $request->all();
        $input['contrasena'] = Hash::make($input['contrasena']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        return response()->json([
            'message' => 'inserted correctly'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->input('id'),
            'contrasena' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['contrasena'])){ 
            $input['contrasena'] = Hash::make($input['contrasena']);
        }else{
            $input = Arr::except($input,array('contrasena'));    
        }

        $user = User::find($input["id"]);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$input["id"])->delete();

        $user->assignRole($request->input('roles'));

        return response()->json([
            'message' => 'User updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        User::find($request->input('id'))->delete();
        return response()->json([
            'message' => 'User deleted successfully'
        ]);
    }
}